from django.conf.urls import patterns, include, url
import os.path
STATIC_ROOT=os.path.join(os.path.dirname(__file__),'templates/static')

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from datapp.views import *
urlpatterns = patterns('',
    # Examples:
     url(r'^$', fun),
     url(r'^db/$',user),
     #url(r'^register/$',fun1),
     url(r'^register/$', fun1),
     url(r'^db1/$',fun2),
     url(r'^logout/$',logout),
     url(r'^del/$',delete),
     url(r'^reg/$',fun3),
     url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',

       {'document_root': STATIC_ROOT}),

     #url(r'^db1/$',fun3),
     #url(r'^db1$',user1),


    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
